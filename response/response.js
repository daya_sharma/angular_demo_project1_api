

module.exports = {

    // create a common response for all apis
    response: (req, res, error, result) => {
        // console.log('result',result)
        console.log('error', error)
        if (error) {
            res.json({

                "Error": error
            })
        }
        else {
            {
                if (result.rowCount <= 0) {
                    if (!error) {
                        res.json({
                            "result": result.rows,
                        })
                    }
                    else {
                        res.json({
                            "error": error
                        })
                    }

                }
                else {
                    //res.json({ "msg":"success","data": result.rows }) 
                    res.json(result.rows)
                }


            }
        }

    },


}