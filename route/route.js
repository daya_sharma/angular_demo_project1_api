var express=require('express');
var app=express();      // create a common variable for express

// locate api file where each api is located 
var api=require('../controller/api');

// call the api function
app.get('/getUserDetails/:id?', function (req, res) {
  api.getUserDetails(req, res)
});

// deleteRecords
app.post('/deleteRecords', function (req, res) {
  api.deleteRecords(req, res)
});

// addORUpdateRecords
app.post('/addORUpdateRecords', function (req, res) {
  api.addORUpdateRecords(req, res)
});


//define dependencies and modules  (npm ecosystem) 
module.exports=app